class ConfigRetriever {
    subtypes;
    basetypes;

    retrieveBaseTypes = () => {
        let classInstance = this;
        return fetch('/fhir-resources/basetypes.json')
        .then(response => response.json())
        .then(config => {
            classInstance.basetypes = config;
        });
    }

    retrieveSubTypes = () => {
        let classInstance = this;
        return fetch('/fhir-resources/subtypes.json')
        .then(response => response.json())
        .then(config => {
            classInstance.subtypes = config;
        });
    }

    generateUniqueId = () => {
        return 'id-' + Math.random().toString(36).substr(2, 16);
    }

    retrieveConfigurationForResource = (resourceType) => {
        let classInstance = this;
        let config = this.basetypes[resourceType];
        let resultConfig = {
            resourceType: resourceType,
            formElements: []
        };

        classInstance.setElementConfiguration(resultConfig, config);
        return resultConfig;
    }

    // Some recursive voodoo along with getElementConfig
    setElementConfiguration = (resultConfig, parentElement, parentName) => {
        for (let i = 0; i < parentElement.formElements.length; i++) {
            this.getElementConfig(resultConfig, parentElement.formElements[i], parentName);
        }
    }

    getElementConfig = (resultConfig, element, parentName) => {
        if (element.type === 'subtype') {
            let subtypeName = element.subtypeType;
            let subtypeConfig = this.subtypes[subtypeName];

            let name = element.name;
            if (parentName) {
                name = parentName + "." + element.name;
            }
            this.setElementConfiguration(resultConfig, subtypeConfig, name);
        } else {
            let name;

            if (parentName) {
                name = parentName + '.' + element.name;
            } else {
                name = element.name;
            }

            let newConfig = Object.assign({}, element);
            newConfig.id = this.generateUniqueId();
            newConfig.name = name;
            resultConfig.formElements.push(newConfig);
        }
    }
}

export default ConfigRetriever;