import React from 'react';

class SelectInput extends React.Component {
    render() {
        return (
            <select id={this.props.id} placeholder={this.props.placeholder}>
                <option value="">{this.props.placeholder}</option>
                { this.props.options.map(element => <option key={element.value} value={element.value}>{element.display}</option>)}
            </select>
        );
    }
}

export default SelectInput;