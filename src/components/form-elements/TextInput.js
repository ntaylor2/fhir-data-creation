import React from 'react';

class TextInput extends React.Component {
    
    render() {
        let label = '';
        if (this.props.label) {
            label = <label htmlFor={this.props.id}>{this.props.label}</label>;
        }

        return (
            <div className="input-group-element">
                {label}
                <input type="text" name={this.props.name} placeholder={this.props.placeholder} id={this.props.id}/>
            </div>
        );
    }
}

export default TextInput;