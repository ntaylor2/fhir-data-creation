import React from 'react';

class CheckboxInput extends React.Component {
    render() {
        return (
            <div className="checkbox-input">
                <label htmlFor={this.props.id}>{this.props.label}</label>
                <input type="checkbox" name={this.props.name} id={this.props.id}/>
            </div>
        );
    }
}

export default CheckboxInput;