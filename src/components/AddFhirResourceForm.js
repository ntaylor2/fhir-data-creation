import React from 'react';
import TextInput from './form-elements/TextInput';
import SelectInput from './form-elements/SelectInput';
import CheckboxInput from './form-elements/CheckboxInput';
import set from 'lodash/set';
import get from 'lodash/get';

class AddFhirResourceForm extends React.Component {

    fhirResourceUrl = 'http://localhost:8080/hapi-fhir-jpaserver-example/baseDstu3';

    addFhirResource = (event) => {
        event.preventDefault();
        let json = this.buildFormSubmission();
        const resourceType = this.props.resourceConfig.resourceType
        json.resourceType = resourceType;
        this.saveResourceToServer(resourceType, json);
    }

    saveResourceToServer = (resourceType, json) => {
        let url = `${this.fhirResourceUrl}/${resourceType}?_format=json`;
        fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(json)
        })
        .then(response => response.json())
        .then(response => {
            if (response.issue && response.issue.length > 0 && response.issue[0].severity === 'information') {
                this.props.saveSuccess();
            } else {
                this.props.saveFailure(get(response, 'issue[0].diagnostics'));
            }
        });
    }

    getFormComponent = (element) => {
        switch(element.type) {
            case 'text':
                return <TextInput key={element.id} id={element.id} placeholder={element.placeholder} name={element.name} label={element.label}/>
            case 'select': 
                return <SelectInput key={element.id} id={element.id} placeholder={element.placeholder} name={element.name} options={element.options}/>
           case 'checkbox':
                return <CheckboxInput key={element.id} id={element.id} label={element.label}/>
            default:
                console.error(`Element type "${element.type}" is not supported`);
                return '';
        }
    }

    buildFormSubmission = () => {
        const baseFormElements = this.props.resourceConfig.formElements;
        let jsonSubmission = {};

        for (let i = 0; i < baseFormElements.length; i++) {
            const el = baseFormElements[i];
            const elemValue = this.findElementValue(el.id, el.type);
            set(jsonSubmission, el.name, elemValue);
        }

        return jsonSubmission;
    }

    findElementValue = (id, type) => {
        if (type === 'checkbox') {
            return document.getElementById(id).checked;
        }
        
        return document.getElementById(id).value;
    }

    render() {
        if (this.props.resourceConfig === '') {
            return '';
        } else {
            let formElements = this.props.resourceConfig.formElements;
            return (
                <div className="add-resource-form-wrapper">
                    <form className="add-fhir-resource-form" onSubmit={this.addFhirResource}>
                        { formElements.map(element => this.getFormComponent(element))}
                        <button type="submit">Submit</button>
                    </form>
                </div>
            );
        }
        
    }
}

export default AddFhirResourceForm;