import React from 'react';

class ResourceButton extends React.Component {

    buttonClicked = (event) => {
        event.preventDefault();
        this.props.showAddForm(this.props.resourceType);
    }

    render() {
        return (
            <button type="button" className="btn btn-success resource-button" onClick={this.buttonClicked}>{this.props.displayName}</button>
        );
    }
}

export default ResourceButton;