import React from 'react';
import ResourceButton from './ResourceButton';
import AddFhirResourceForm from './AddFhirResourceForm';
import ConfigRetriever from '../config/ConfigRetriever';

class App extends React.Component {
    configRetriever;
    state = {
        selectedResourceConfig: '',
        selectedResourceType: '',
        saveClassName: '',
        saveMessage: ''
    };

    componentDidMount() {
        this.configRetriever = new ConfigRetriever();
        this.configRetriever.retrieveBaseTypes();
        this.configRetriever.retrieveSubTypes();
    }

    showAddForm = (resourceType) => {
        this.setState({
            saveClassName: '',
            saveMessage: ''
        });
        const resultConfig = this.configRetriever.retrieveConfigurationForResource(resourceType)
        this.setState({
            selectedResourceConfig: resultConfig,
            selectedResourceType: resourceType
        });
    }

    saveSuccess = () => {
        this.setState({
            selectedResourceConfig: '',
            saveClassName: 'success',
            saveMessage: 'Saved Successfully'
        });
    }

    saveFailure = (error) => {
        this.setState({
            saveClassName: 'failure',
            saveMessage: error
        });
    }

    render() {
        return (
            <div className="base-div">
                <h1 className="resource-buttons-header">Resources Available</h1>
                <hr className="my-4"></hr>
                <div className={this.state.saveClassName}>
                    <p>{this.state.saveMessage}</p>
                </div>
                <div className="resource-buttons">
                    <ResourceButton displayName="Patient" resourceType="Patient" showAddForm={this.showAddForm}/>
                    <ResourceButton displayName="Practitioner" resourceType="Practitioner" showAddForm={this.showAddForm}/>
                    <ResourceButton displayName="Organization" resourceType="Organization" showAddForm={this.showAddForm}/>
                    <ResourceButton displayName="Condition" resourceType="Condition" showAddForm={this.showAddForm}/>
                </div>
                <hr className="my-4"></hr>
                <AddFhirResourceForm resourceConfig={this.state.selectedResourceConfig} saveSuccess={this.saveSuccess} saveFailure={this.saveFailure} />
            </div>
        )
    }
}

export default App;